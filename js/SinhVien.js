


function SinhVien(_maSinhVien, _tenSinhVien, _loaiSinhVien, _diemToan, _diemVan){
    
    this.maSinhVien = _maSinhVien;
    this.tenSinhVien = _tenSinhVien;
    this.loaiSinhVien = _loaiSinhVien;
    this.diemToan = _diemToan;
    this.diemVan = _diemVan;

    this.TinhDiemTB = function () {
        return (this.diemToan + this.diemVan) / 2;

    }

    this.XepLoai = function () {
        var diemTB = this.TinhDiemTB();
        if (diemTB > 8){
            XepLoai = "gioi"
        } else {
            XepLoai = "do"
        }
        return XepLoai;
    }
}

     
