var mangSinhVien = [];
function themSinhVien() {
    
    var maSV = document.getElementById("txtMaSV").value;
    var tenSV = document.getElementById("txtTenSV").value;
    var loaiSV = document.getElementById("loaiSV").value;
    var diemToan = parseFloat(document.getElementById("txtDiemToan").value);
    var diemLy = parseFloat(document.getElementById("txtDiemLy").value);
    var diemHoa = parseFloat(document.getElementById("txtDiemHoa").value);
    var diemRL = parseFloat(document.getElementById("txtDiemRenLuyen").value);



    var sinhVien = {
        maSinhVien : maSV,
        tenSinhVien : tenSV,
        loaiSinhVien : loaiSV,
        diemToan : diemToan,
        diemLy : diemLy,
        diemHoa : diemHoa,
        diemRL : diemRL,
        TinhDiemTB : function () {
            return (this.diemToan + this.diemHoa + this.diemLy) / 3;
        }
    } 


    
    mangSinhVien.push(sinhVien);
    HienThi();
    
    
    
    function HienThi() {
        var content = "";
        var tbodySinhVien = document.getElementById("tbodySinhVien");
        for (var i= 0; i < mangSinhVien.length; i++){
            var sinhVien = mangSinhVien[i];
            content += `
                <tr>
                    <td>${sinhVien.maSinhVien}</td>
                    <td>${sinhVien.tenSinhVien}</td>
                    <td>${sinhVien.loaiSinhVien}</td>
                    <td>${sinhVien.TinhDiemTB()}</td>
                    <td>${sinhVien.diemRL}</td>
                </tr>
            `
        }
        tbodySinhVien.innerHTML = content;
    }
}



